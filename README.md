# LendFlow.io Todo List Code Challenge
![Coverage](https://gitlab.com/bk-lendflow/app/-/raw/master/coverage.svg)

## Code Structure

The application is separated into 3 modules. `app`, `core`, `ui`.

- `app`  - [App Repo](https://gitlab.com/bk-lendflow/app)   - This is the main application that brings all the modules together. In app/composer.json you can see that we've required the other two packages `bk-lendflow/core`, and `bk-lendflow/ui`.
- `core` - [Core Repo](https://gitlab.com/bk-lendflow/core) - This package contains all the classes that can be shared by other modules such as base classes. In this code challenge, I could have go away and avoid using the `core` but I've included it as an example.
- `ui`   - [UI Repo](https://gitlab.com/bk-lendflow/ui)     - This is where the user interface resides. Since the `app` module is purely written as an API it would need a UI. I've used Vitejs for this exercise.

## Host file entries
This exercise requires the developer to add an entry to their host file (/etc/hosts) for linux and mac.
- 127.0.0.1 todo.lendflow.local

Please note if you're using homestead that you need to add the ip address of your homestead instance instead of `127.0.0.1`. Usually this value would be `192.168.10.10`.
This part is vital since the frontend ui will send requests to this local domain.

## Installation
- git clone git@gitlab.com:bk-lendflow/app.git app
- cd app
- cp .env.example .env
- // modify .env file as needed. Especially DB_ variables.
- composer install
- php artisan migrate

Once you've completed the installation process and the host file entries, you can now go to http://todo.lendflow.local in your browser.

- If done correctly, an ignition error page will show, stating that the UI Assets are missing.
- Kindly click the `Publish LendFlow UI assets` button or run the command `php artisan vendor:publish --tag=lendflow::ui` in the root of the `app` directory.
- Refresh the page and the App should start normally.

## Publishing UI

The application comes with a separate user interface. The first party UI uses the Vitejs Framework. In order to gain leverage on its usage, you need to go in your terminal and navigate to the root of the `app` directory, then run the artisan command below.

`php artisan vendor:publish --tag=lendflow::ui`

## Testing

Run the command in the command line and the phpunit will run. It will also create a coverage and save the coverage.svg to the root of the project. The coverage can also be viewed locally in the build/coverage/index.html file.

```sh
$ ./phpunit.sh
```
