<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Http\Requests\ItemRequest;
use App\Http\Resources\ItemResource as Resource;

class ItemController extends Controller
{
    public function store(ItemRequest $request)
    {
        $data = $request->only('title', 'description');

        $item = Item::create($data);

        return new Resource($item);
    }

    public function update(ItemRequest $request, Item $item)
    {
        $data = $request->only('title', 'description');

        $item = tap($item)->update($data);

        return new Resource($item);
    }

    public function destroy(ItemRequest $request, Item $item)
    {
        $item->delete();

        return response()->json(['status' => 'deleted']);
    }
}
