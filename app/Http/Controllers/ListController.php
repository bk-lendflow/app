<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Http\Requests\ListRequest;

class ListController extends Controller
{
    public function update(ListRequest $request, Item $item)
    {
        $list = collect($request->all())->map(function ($item) {
            return [
                'title' => $item['title'],
                'description' => $item['description'],
                'created_at' => now(),
                'updated_at' => now(),
            ];
        });

        Item::truncate();

        Item::insert($list->toArray());

        return response()->json(['status' => 'updated']);
    }
}
