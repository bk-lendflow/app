<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ListController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'items'], function () {
    Route::post('', [ItemController::class, 'store'])->name('item.store');
    Route::patch('{item}', [ItemController::class, 'update'])->name('item.update');
    Route::delete('{item}', [ItemController::class, 'destroy'])->name('item.destroy');
});

Route::group(['prefix' => 'lists'], function () {
    Route::patch('', [ListController::class, 'update'])->name('list.update');
});
