<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Item;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function item_title_and_description_must_be_at_least_5_characters()
    {
        $data = [
            'title' => 'this',
            'description' => 'desc',
        ];

        $response = $this->json('post', '/api/items', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'title' => [],
                    'description' => [],
                ],
            ]);

        $responseContent = json_decode($response->getContent(), true);

        $this->assertEquals('The given data was invalid.', $responseContent['message']);
        $this->assertEquals('The title must be at least 5 characters.', $responseContent['errors']['title'][0]);
        $this->assertEquals('The description must be at least 5 characters.', $responseContent['errors']['description'][0]);
    }

    /** @test */
    public function item_title_and_description_are_required_fields()
    {
        $data = [];

        $response = $this->json('post', '/api/items', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'title' => [],
                    'description' => [],
                ],
            ]);

        $responseContent = json_decode($response->getContent(), true);

        $this->assertEquals('The given data was invalid.', $responseContent['message']);
        $this->assertEquals('The title field is required.', $responseContent['errors']['title'][0]);
        $this->assertEquals('The description field is required.', $responseContent['errors']['description'][0]);
    }

    /** @test */
    public function can_add_an_item()
    {
        $data = [
            'title' => 'This is title',
            'description' => 'A very good description',
        ];

        $response = $this->json('post', '/api/items', $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'description',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $responseContent = json_decode($response->getContent(), true);
        $data = $responseContent['data'];

        $this->assertEquals('This is title', $data['title']);
        $this->assertEquals('A very good description', $data['description']);
    }

    /** @test */
    public function can_update_an_item_title()
    {
        $data = [
            'title' => 'This is title',
            'description' => 'A very good description',
        ];

        $item = Item::factory()->state($data)->create();

        $data = [
            'title' => 'The best title',
        ];

        $response = $this->json('patch', "/api/items/{$item->id}", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'description',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $responseContent = json_decode($response->getContent(), true);
        $data = $responseContent['data'];

        $this->assertEquals('The best title', $data['title']);
        $this->assertEquals('A very good description', $data['description']);
    }

    /** @test */
    public function can_update_an_item_description()
    {
        $data = [
            'title' => 'This is title',
            'description' => 'A very good description',
        ];

        $item = Item::factory()->state($data)->create();

        $data = [
            'description' => 'The best description',
        ];

        $response = $this->json('patch', "/api/items/{$item->id}", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'description',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $responseContent = json_decode($response->getContent(), true);
        $data = $responseContent['data'];

        $this->assertEquals('This is title', $data['title']);
        $this->assertEquals('The best description', $data['description']);
    }

    /** @test */
    public function can_delete_an_item()
    {
        $data = [
            'title' => 'This is title',
            'description' => 'A very good description',
        ];

        $item = Item::factory()->state($data)->create();

        $data = [
            'description' => 'The best description',
        ];

        $response = $this->json('delete', "/api/items/{$item->id}", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'status',
            ]);

        $responseContent = json_decode($response->getContent(), true);

        $this->assertEquals('deleted', $responseContent['status']);
    }
}
