<?php

namespace Tests\Feature;

use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_update_list()
    {
        $data = [
            [
                'title' => 'first title',
                'description' => 'first description',
            ],
        ];

        $response = $this->json('patch', '/api/lists', $data);

        $response->dump();

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'status',
            ]);

        $responseContent = json_decode($response->getContent(), true);

        $this->assertEquals('updated', $responseContent['status']);
    }
}
